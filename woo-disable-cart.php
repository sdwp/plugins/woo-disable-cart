<?php

/**
	* Отключение корзины в WooCommerce
	*
	* Временное или постоянное отключение функционала корзины WooCommerce
	* Превращение в каталог
	*
	*
	*
	* @link              https://gitlab.com/sdwp/plugins/woo-disable-cart
	* @since             1.0.0
	* @package           woo_disable_cart
	*
	* @wordpress-plugin
	* Plugin Name:       WooCommerce Disable cart
	* Plugin URI:        https://gitlab.com/sdwp/plugins/woo-disable-cart
	* Description:       Временное или постоянное отключение функционала корзины WooCommerce
	* Version:           1.0.0
	* Author:            Sergey Davydov <dev@sdcollection.com>
	* Author URI:        https://gitlab.com/MrSwed https://gitрги.com/MrSwed
	* License:           GPL-2.0+
	* License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
	* Text Domain:       woo-disable-cart
	* Domain Path:       /languages
	*/

function remove_storefront_header_cart() {
// https://wordpress.org/support/topic/problem-with-hooks-causing-content-updating-error/
	remove_action( 'storefront_header', 'storefront_header_cart', 30 );
}

function woo_disable_mobile_cart($links){
	unset ($links["cart"]);
	return $links;
}

function woo_disable_cart_init() {
	/** https://stackoverflow.com/a/29314343/5447232 */
	add_filter( 'woocommerce_is_purchasable', '__return_false', 10, 2 );
	add_filter( 'woocommerce_widget_cart_is_hidden', '__return_true', 100 );

	add_filter( 'storefront_handheld_footer_bar_links', 'woo_disable_mobile_cart');

	add_action( 'storefront_before_header', 'remove_storefront_header_cart' );

	remove_action( 'storefront_header', 'storefront_header_cart', 30 );
	remove_action( 'storefront_header', 'storefront_header_cart', 60 );
}

add_action( 'init', 'woo_disable_cart_init', 1000 );
